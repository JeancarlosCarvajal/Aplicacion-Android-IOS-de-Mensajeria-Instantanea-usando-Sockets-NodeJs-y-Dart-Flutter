


// import 'dart:io';

import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';

class Environment {

  // static Uri apiUrl = Uri.http(
  //   Platform.isAndroid ? '192.168.1.5:3000' :  'localhost:3000', // authority del http
  //   '/api' // unencodedPath
  // );
  
  // de pruebas en localhost... OJOJOJO e localhost es Uri.http()
  // static const _hostIp   = '192.168.1.5:3000';
  // static const _hostName = 'localhost:3000'; // https://desarrollo-web3.com/

  // Produccion en heroku
  // static  const String _hostIp  = 'flutter-chat-server-socket.herokuapp.com'; // https://flutter-chat-server-socket.herokuapp.com/
  // static const String _hostName = 'flutter-chat-server-socket.herokuapp.com';
  
  // static  const String _hostIp  = 'desarrollo-web3.com'; // api-chat.desarrollo-web3.com
  // static const String _hostName = 'desarrollo-web3.com'; 

  static  const String _hostIp  = 'api-chat.desarrollo-web3.com'; // 
  static const String _hostName = 'api-chat.desarrollo-web3.com'; 

  // hice esta funcion para factorizar codigo y manejar los URI de manera dinamica
  // lo que quedan debajo son de muestra, se pueden refactorizar con esta funcion y borrarlos para reducir codigo
  static bool isAndroid = (defaultTargetPlatform == TargetPlatform.android);

  // lo que quedan debajo son de muestra, se pueden refactorizar con esta funcion y borrarlos para reducir codigo
  static Uri apiUrl(String path) {
    return Uri.https(
      isAndroid ? _hostIp :  _hostName, // authority del http
      '/api/$path' // unencodedPath
    );
  }

  static Uri apiUrlRenew = Uri.https(
    isAndroid ? _hostIp :  _hostName, // authority del http
    '/api/login/renew' // unencodedPath
  );

  static Uri apiUrlRegister = Uri.https(
    isAndroid ? _hostIp :  _hostName, // authority del http
    '/api/login/new' // unencodedPath
  );

  static Uri apiUrlLogin = Uri.https(
    isAndroid ? _hostIp :  _hostName, // authority del http
    '/api/login' // unencodedPath
  );

  static Uri socketUrl = Uri.https( 
    isAndroid ? _hostIp :  _hostName, 
    // 'desarrollo-web3.com',
    '/'
  );

  // static String apiUrl    = Platform.isAndroid ? 'http://192.168.1.5:3000/api' :  'http://localhost:3000/api'; // flutter viejo 2
  // static String socketUrl = Platform.isAndroid ? 'http://192.168.1.5:3000' :  'http://localhost:3000'; // flutter viejo 2
    

}

// Exception has occurred.
// FormatException (FormatException: Illegal IPv6 address, an IPv6 part can only contain a maximum of 4 hex digits (at character 1)
// 192.168.1.5:3000 )

// documentation
/*
    `(new) Uri Uri.http(
    String authority, String unencodedPath, Map<String, dynamic>? queryParameters,)`

    Creates a new http URI from authority, path and query.

    Example:

    var uri = Uri.http('example.org', '/path', { 'q' : 'dart' });
    print(uri); // http://example.org/path?q=dart

    uri = Uri.http('user:password@localhost:8080', '');
    print(uri); // http://user:password@localhost:8080

    uri = Uri.http('example.org', 'a b');
    print(uri); // http://example.org/a%20b

    uri = Uri.http('example.org', '/a%2F');
    print(uri); // http://example.org/a%252F
    The scheme is always set to http.

    The userInfo, host and port components are set from the authority argument. If authority is null or empty, the created Uri has no authority, and isn't directly usable as an HTTP URL, which must have a non-empty host.

    The path component is set from the unencodedPath argument. The path passed must not be encoded as this constructor encodes the path.

    The query component is set from the optional queryParameters argument.

    Open library docs 

*/