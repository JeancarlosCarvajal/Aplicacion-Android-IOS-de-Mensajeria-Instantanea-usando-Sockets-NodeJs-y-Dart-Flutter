import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:b_chat_flutter_app/services/auth_service.dart';

class ChatMessage extends StatelessWidget {

  final String texto;
  final String uid;

// para usar el animation controler del chat_message se debe agregar with TickerProviderStateMixin
// al widget donde lo voy a invocar hacia aca
  final AnimationController animationController;
  
  const ChatMessage({Key? key, 
    required this.texto, 
    required this.uid, 
    required this.animationController // existe en flutter atrasar adelantar
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    // accedemos al provider para conectarnos quien soy yo
    final authService = Provider.of<AuthService>(context, listen: false);
    print('de: $uid - para: ${authService.usuarios.uid}');
    
    return FadeTransition(
      opacity: animationController,
      child: SizeTransition(
        sizeFactor: CurvedAnimation(parent: animationController, curve: Curves.easeInOut),
        child: Container(
          child: this.uid == authService.usuarios.uid // de == para '123' es mi id lo uso de pruebas, algo parecido a lo que hice en buscotengo
            ? _myMessage()
            : _notMyMessage()
        ),
      ),
    );
  }
  
  Widget _myMessage() {
    return Container(
      alignment: Alignment.centerRight,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.only(
          right: 20,
          bottom: 5,
          left: 50
        ),
        decoration:  BoxDecoration(
          color: const Color(0xff4d9ef6),
          borderRadius: BorderRadius.circular(10)
        ),
        child: Text(texto, style: const TextStyle(color: Colors.white)),
      ),
    );
  }
  
  Widget _notMyMessage() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.only(
          right: 50,
          bottom: 5,
          left: 20
        ),
        decoration:  BoxDecoration(
          color: const Color(0xffe4e5e8),
          borderRadius: BorderRadius.circular(10)
        ),
        child: Text(texto, style: const TextStyle(color: Colors.black87)),
      ),
    );
  }
}