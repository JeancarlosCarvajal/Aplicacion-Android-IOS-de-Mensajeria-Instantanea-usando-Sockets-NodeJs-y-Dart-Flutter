import 'package:flutter/material.dart';

class BotonAzul extends StatelessWidget {
  final String text;
  final void Function()? onPressed;

  const BotonAzul({Key? key, required this.text, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var focusNode = FocusNode();
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 25),
      child: MaterialButton(
        focusNode: focusNode,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        disabledColor: Colors.grey,
        elevation: 5,
        minWidth: 50,
        color: Colors.deepPurple,

        onPressed: onPressed,
        
        // onPressed: () { // una forma de enviar mensajes personalizado de error o cualquier cosa
        //   Scaffold.of(context).showBottomSheet<void>(
        //     (BuildContext context) {
        //       return Container(
        //         alignment: Alignment.center,
        //         height: 200,
        //         color: Colors.yellow[400],
        //         child: Center(
        //           child: Column(
        //             mainAxisSize: MainAxisSize.min,
        //             children: <Widget>[
        //               const Text('BottomSheet'),
        //               ElevatedButton(
        //                 child: const Text('Close BottomSheet'),
        //                 onPressed: () {
        //                   Navigator.pop(context);
        //                 },
        //               )
        //             ],
        //           ),
        //         ),
        //       );
        //     },
        //   );
        // },
        child: SizedBox(
          width: double.infinity,
          height: 55,
          child: Center(
            child: Text(text,
                style: const TextStyle(color: Colors.white, fontSize: 17)),
          ),
        ),
      ),
    );
  }
}
