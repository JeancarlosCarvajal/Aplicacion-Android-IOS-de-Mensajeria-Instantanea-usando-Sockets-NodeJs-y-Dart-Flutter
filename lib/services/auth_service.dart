import 'dart:convert';

import 'package:b_chat_flutter_app/global/environment.dart';
import 'package:b_chat_flutter_app/models/login_responce.dart';
import 'package:b_chat_flutter_app/models/usuario.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;




class AuthService extends ChangeNotifier {

  // Create storage
  final _storage = new FlutterSecureStorage();
 
  late Usuario usuarios;

  // verifica mientrar se autentica
  bool _autenticando = false;

  // verifica mientrar se registra
  bool _registrando = false;

  // obtiene si esta autenticando
  bool get autenticando => this._autenticando;

  // obtiene si esta registrando
  bool get registrando => this._registrando;
  
  // setea cuando esta autenticando
  set setAutenticando( bool valor ){
    this._autenticando = valor;
    // notifica a todos que esta autenticado
    notifyListeners();
  }
  
  // setea cuando esta registrando
  set setRegistrando( bool valor ){
    this._registrando = valor;
    // notifica a todos que esta autenticado
    notifyListeners();
  }

  // Getters del token de forma estatica
  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token ?? '';
  }

  // Getters del token de forma estatica
  static Future<void> deleToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token'); 
  }

  // registro de usuario
  Future<bool> login(String email, String password) async {
    // setemos el autenticando en true
    this.setAutenticando = true;

    final data = {
      'email': email,
      'password': password
    };
     

    final resp = await http.post(Environment.apiUrlLogin,
      body: jsonEncode(data), // aqui va el usuario y la contrasena
      headers: {
        'Content-Type': 'application/json'
      }
    );

    // ya me llego la respuesta por lo tanto seteo en false el autenticando
    this.setAutenticando = false;

    print(resp.body);
    
    // verificar la respuesta
    if(resp.statusCode == 200){
      final loginResponce = LoginResponce.fromJson(resp.body);
      this.usuarios = loginResponce.usuario;
  
      // Guardar token en lugar seguro
      await this._guardarToken(loginResponce.token);

      return true;
    }else{
      return false;
    }


  }

  // registro del usuario
  Future<dynamic> register(String name, String email, String password) async {
    // setemos el autenticando en true
    this.setRegistrando = true;

    final data = {
      'nombre': name,
      'email': email,
      'password': password
    };
      

    final resp = await http.post(Environment.apiUrlRegister,
      body: jsonEncode(data), // aqui va el usuario y la contrasena
      headers: {
        'Content-Type': 'application/json'
      }
    );

    // ya me llego la respuesta por lo tanto seteo en false el autenticando
    this.setRegistrando = false;

    print(resp.body);
    
    // verificar la respuesta
    if(resp.statusCode == 200){ 
      final registerResponce = LoginResponce.fromJson(resp.body);
      this.usuarios = registerResponce.usuario;

      // Guardar token en lugar seguro
      await this._guardarToken(registerResponce.token);

      return true;
    }else{
      final respBody = jsonDecode(resp.body);
      return respBody['msg'];
    }
  }

  // verifica si esta logeado y tiene token el usuario
  Future<bool> isLogedIn() async {

    final token = await this._storage.read(key: 'token');
    print(token);
    if(token == null) return false;

    final resp = await http.get(Environment.apiUrlRenew, 
      headers: {
        'Content-Type': 'application/json',
        'x-token': token
      }
    ); 
     // es una forma de enviar datos usando el header

    print(resp.body);
    
    // verificar la respuesta
    if(resp.statusCode == 200){ 
      final registerResponce = LoginResponce.fromJson(resp.body);
      this.usuarios = registerResponce.usuario;

      // Guardar token NUEVO en lugar seguro
      await this._guardarToken(registerResponce.token);

      return true;
    }else{
      // borro el token porque ya expiro o no sirve
      // por activar
      this.logOut();
      print('No tengo token valido');
      
      return false;
    }  
  }

  // guarda el token en el storage del dispositivo
  Future _guardarToken( String token ) async {  
    return await _storage.write(key: 'token', value: token); 
  }

  // al hacer loout me borra el token del storage
  Future logOut() async {
    return await _storage.delete(key: 'token'); 
  }


}

// documentation
/*
    `(new) Uri Uri.http(
    String authority, String unencodedPath, Map<String, dynamic>? queryParameters,)`

    Creates a new http URI from authority, path and query.

    Example:

    var uri = Uri.http('example.org', '/path', { 'q' : 'dart' });
    print(uri); // http://example.org/path?q=dart

    uri = Uri.http('user:password@localhost:8080', '');
    print(uri); // http://user:password@localhost:8080

    uri = Uri.http('example.org', 'a b');
    print(uri); // http://example.org/a%20b

    uri = Uri.http('example.org', '/a%2F');
    print(uri); // http://example.org/a%252F
    The scheme is always set to http.

    The userInfo, host and port components are set from the authority argument. If authority is null or empty, the created Uri has no authority, and isn't directly usable as an HTTP URL, which must have a non-empty host.

    The path component is set from the unencodedPath argument. The path passed must not be encoded as this constructor encodes the path.

    The query component is set from the optional queryParameters argument.

    Open library docs 

*/