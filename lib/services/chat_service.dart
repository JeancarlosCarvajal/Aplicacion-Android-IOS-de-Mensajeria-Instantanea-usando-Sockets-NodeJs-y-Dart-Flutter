import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:b_chat_flutter_app/global/environment.dart';
import 'package:b_chat_flutter_app/services/auth_service.dart';

import 'package:b_chat_flutter_app/models/mensajes_response.dart';
import 'package:b_chat_flutter_app/models/usuario.dart';



class ChatService extends ChangeNotifier{

  // El Receptor del mensaje
  late Usuario usuarioPara;

  // hacemos la peticion del los mensajes
  Future<List<Mensaje>> getChat ( String usuarioUid, {String inicio = '0'} ) async {
    
    try {
      final resp = await http.get(Environment.apiUrl('mensajes/$usuarioUid'), 
        headers: {
          'Content-Type': 'application/json',
          'inicio': inicio,
          'x-token': await AuthService.getToken()
        }
      );
      print(resp.statusCode);

      if(resp.statusCode != 200) return [];
      
      // parseamos la respuesta a nuestro modelo creado
      final MensajesResponse mensajesResponse = MensajesResponse.fromJson(resp.body);

      print(mensajesResponse.mensajes);
      
      // retornamos la respuesta de la lista de usuarios
      return mensajesResponse.mensajes;

    } catch (e) {
      return [];
    }
  }

}