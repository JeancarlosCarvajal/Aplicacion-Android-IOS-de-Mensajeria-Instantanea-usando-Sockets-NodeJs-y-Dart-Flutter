import 'package:b_chat_flutter_app/models/usuarios_responce.dart';
import 'package:http/http.dart' as http;

import 'package:b_chat_flutter_app/models/usuario.dart';
import 'package:b_chat_flutter_app/global/environment.dart';

import 'package:b_chat_flutter_app/services/auth_service.dart';

// no lo voy a extender de ChangeNotifier para hacerlo de otra manera
class UsuariosService {

  // getUsuarios puede recibir la pagina en caso que se requiera
  Future <List<Usuario>> getUsuarios( {String inicio = '0'} ) async {// Environment.apiUrlRenew

    // print(Environment.apiUrl('usuarios'));

    try {
      final resp = await http.get(Environment.apiUrl('usuarios/'), 
        headers: {
          'Content-Type': 'application/json',
          'inicio': inicio,
          'x-token': await AuthService.getToken()
        }
      ); 
      // print(resp.statusCode);
      if(resp.statusCode != 200) return [];
      
      // parseamos la respuesta a nuestro modelo creado
      final UsuariosResponse usuariosResponse = UsuariosResponse.fromJson(resp.body);
      // print(usuariosResponse.usuarios);
      
      // retornamos la respuesta de la lista de usuarios
      return usuariosResponse.usuarios;

    } catch (e) {
      return [];
    }
  }


}