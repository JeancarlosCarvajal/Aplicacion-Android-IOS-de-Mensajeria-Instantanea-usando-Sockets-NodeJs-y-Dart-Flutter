import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as io; 
import 'package:b_chat_flutter_app/services/auth_service.dart';

enum ServerStatus {
  Online,
  Offline,
  Connecting
}

class SocketService extends ChangeNotifier {

  ServerStatus _serverStatus = ServerStatus.Connecting;
  late io.Socket _socket;

  ServerStatus get serverStatus => this._serverStatus;
  io.Socket get socket => this._socket;
  Function get emit => this.socket.emit;


  // Inicamos la conexion
  void connect() async {

    // buscamos el token porque en toria el ususario ya esta autenticado
    // para enviarlo por sockets y validar la conexion del socket
    final token = await AuthService.getToken();



    // Dart client... ipconfig en la cmd: aqui busque mi ipv4 y cambie el localhost por mi ip 192.168.1.5
    // IPv4 Address. . . . . . . . . . . : 192.168.1.5          ${Environment.socketUrl}
    // 'forceNew': true es para que force a conectar el socket cuando el cliente cambie de token
    // Documentacion https://socket.io/docs/v3/client-initialization/ 
    // https://socket.io/docs/v3/handling-cors/
    // TODO: era esto lo que tenia que colocar CSM mil veces. dos dias en este peooo...  ws://desarrollo-web3.com:3000
    // ws://desarrollo-web3.com:3000
    // tambien tuve que modificar el .htaccess ojo con eso ['websocket', 'polling', 'flashsocket'],
    print('jean pidiendo socket: $token');    
    this._socket = io.io('ws://api-chat.desarrollo-web3.com:3000', {// en local tenia mi ip http://192.168.1.5:3000/  ....  https://flutter-sockets-server-jean.herokuapp.com/
      'transports': ['websocket'],
      'autoConnect': true,
      'forceNew': true,
      'auth': { // si funciona en flutterweb y en mobile tambien
        'x-token': token
      },
      'extraHeaders': { // no funciona en flutter web... si en mobile
        'x-token': token
      },
    });
    // this._socket = io.io('ws://api-chat.desarrollo-web3.com:3000',
    //   io.OptionBuilder()
    //     .setAuth({
    //       'x-token': token
    //     })
    //     .setTransports(['websocket']) // for Flutter or Dart VM
    //     // .disableAutoConnect()  // disable auto-connection
    //     .setExtraHeaders ({
    //       'x-token': token
    //     }) // optional
    //     .build()
    // );  
    // el sid es el ID de la sesión, debe incluirse en el parámetro de consulta sid en todas las solicitudes HTTP posteriores
    // la matriz de upgrades contiene la lista de todos los transportes "mejores" que son compatibles con el servidor
    // los valores pingInterval y pingTimeout se utilizan en el mecanismo de latido

    print('Arrancando el Socket');  

    // socket.emit('message',"this is a test");//sending to sender-client only
    // socket.broadcast.emit('message',"this is a test");//sending to all clients except sender
    // socket.broadcast.to('game').emit('message','nice game');//sending to all clients in 'game' room(channel) except sender
    // socket.to('game').emit('message','enjoy the game');//sending to sender client, only if they are in 'game' room(channel)
    // socket.broadcast.to(socketid).emit('message','for your eyes only');//sending to individual socketid
    // io.emit('message',"this is a test");//sending to all clients, include sender
    // io.in('game').emit('message','cool game');//sending to all clients in 'game' room(channel), include sender
    // io.of('myNamespace').emit('message','gg');//sending to all clients in namespace 'myNamespace', include sender
    // socket.emit();//send to all connected clients
    // socket.broadcast.emit();//send to all connected clients except the one that sent the message
    // socket.on();//event listener, can be called on client to execute on server
    // io.sockets.socket();//for emiting to specific clients
    // io.sockets.emit();//send to all connected clients (same as socket.emit)
    // io.sockets.on();//initial connection from a client.

    // _socket.onConnect((_) {
    //   print('Conectando Sockets');
      // this._serverStatus = ServerStatus.Online;
      // notifyListeners();
    // });
    
    _socket.on('connect', (data) {
      print('Conectando Sockets');
      this._serverStatus = ServerStatus.Online;
      notifyListeners();
    });

    _socket.onDisconnect((_) {
      print('Desconectando Socket');
      this._serverStatus = ServerStatus.Offline;
      notifyListeners();
    });

    // // _socket.on('evento')  es una forma de hacerlo 
    // _socket.on('emitir-mensaje', (payload) {
    //   print('emitir-mensaje: $payload');
    //   print('emitir-mensaje: ' + payload['nombre']);
    //   print( payload.containsKey('nombre') ? 'Tengo Nombre' : 'No tengo Nombre' );
    // });




    _socket.onConnectTimeout((data) => print('Tiempo Excedido'));
    _socket.onError((_) => print('Error General'));
    _socket.onConnectError((value) => print('Error al Conectar el Socket: ${value.toString()}'));
    _socket.on('connect_error', (value) => print('Error de Conexion tipo: ${value.toString()}'));
    

  }

  // nos desconectamos del socket
  void disconnect() {
    this._socket.disconnect();
  }

}