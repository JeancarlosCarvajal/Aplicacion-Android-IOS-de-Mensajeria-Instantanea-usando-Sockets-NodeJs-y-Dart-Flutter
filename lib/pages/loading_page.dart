import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:b_chat_flutter_app/services/socket_service.dart';
import 'package:b_chat_flutter_app/services/auth_service.dart';

import 'package:b_chat_flutter_app/pages/login_page.dart';
import 'package:b_chat_flutter_app/pages/usuarios_page.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (context, snapshot) { 
          return const Center(
            child: Text('Espere....'),
          );
        },
      ),
   );
  }

  Future checkLoginState(BuildContext context) async {
    // leer el auth service
    final authService = Provider.of<AuthService>(context, listen: false);
    final socketService = Provider.of<SocketService>(context, listen: false);

    // verificamos si estamos logeados
    final autenticado = await authService.isLogedIn();

    if(autenticado){
      // Conectar al Socket Service
      socketService.connect();
      
      // Navigator.pushReplacementNamed(context, 'usuarios');
      // Evita la transicion de la pantalla
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
        context, 
        PageRouteBuilder(
          pageBuilder: (_,__,___) => const UsuariosPage(),
          transitionDuration: const Duration(milliseconds: 0)
        )
      );
    }else{
      // Navigator.pushReplacementNamed(context, 'login');
      // Evita la transicion de la pantalla
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
        context, 
        PageRouteBuilder(
          pageBuilder: (_,__,___) => const LoginPage(),
          transitionDuration: const Duration(milliseconds: 0)
        )
      );
    }

  }
}