import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
// import 'dart:io'; // para usar Platform .isIOS

import 'package:provider/provider.dart';

import 'package:b_chat_flutter_app/services/chat_service.dart';
import 'package:b_chat_flutter_app/services/socket_service.dart';
import 'package:b_chat_flutter_app/services/auth_service.dart';
import 'package:b_chat_flutter_app/widgets/chat_message.dart';

import 'package:b_chat_flutter_app/models/mensajes_response.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

// para usar el animation controler del chat_message se debe agregar with TickerProviderStateMixin
class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {

  // _textController es para conntrolar el texto del TextField
  final _textController = new TextEditingController();

  // para controlar el FOcus del Teclado del telefono y esas cosas
  final _focusNode = new FocusNode();

  // final AnimationController _animationController = AnimationController(duration: const Duration(milliseconds: 400), vsync: this );

  // creamos la instancia vacia del chatservice para llenarla en el initial state
  late ChatService chatService;

  // creamos la instancia del Sockets vacia
  late SocketService socketService;

  // creamos la instancia del AuthService para saber quien soy yoo
  late AuthService authService;

  // lista de mensajes del chat
  final List<ChatMessage> _messages = [
    // ChatMessage(animationController: AnimationController(duration: const Duration(milliseconds: 400), vsync: this ), texto: 'Hola Mundo', uid: '123'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Msdundo', uid: '123423423'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Msundo', uid: '123'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Munsevsvdo', uid: '123423'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Musdvndo', uid: '123'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Musdvsdvndo', uid: '1234223'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Musdndo', uid: '123'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Musdvndo', uid: '123423'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Musdvsdvsdvndo', uid: '123423'),
    // ChatMessage(animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 400)), texto: 'Hola Msdvsdvundo', uid: '123423'),
  ];

  // Esta escribiendo
  bool _estaEscribiendo = false;

  @override
  void initState() { 
    super.initState(); 
    // accedemos al provider para ver el ususario cargado en el chatService
    this.chatService = Provider.of<ChatService>(context, listen: false); 

    // accedemos al provider para conectarnos al Sockets
    this.socketService = Provider.of<SocketService>(context, listen: false);

    // accedemos al provider para conectarnos con quien soy yo
    this.authService = Provider.of<AuthService>(context, listen: false);

    // escuchamos con el socket los mensajes personales
    this.socketService.socket.on('mensaje-personal', _escucharMensaje);

    // llamar los mensajes guardados
    _cargarHistorial(chatService.usuarioPara.uid);
  }

  Future<void> _cargarHistorial(String usuarioUid) async {
    List<Mensaje> chat = await this.chatService.getChat( usuarioUid );
    // print(chat);
    final history = chat.map((chat) => ChatMessage( // tenia new
      texto: chat.mensaje, 
      uid: chat.de, 
      animationController: AnimationController(duration: const Duration(milliseconds: 200), vsync: this)..forward() // ..forward() es pa que se haga de inmediato
      )
    );
    setState(() {
      _messages.insertAll(0, history);
    });
  } 

  // creamo el metodo para escuchar el socket cuando emita mensajes personales
  void _escucharMensaje (dynamic payload) {
    // print('Tengo mensaje: $payload');
    // Creamos la instancia del mensaje
    final newMessage = ChatMessage( // tenia new
      texto: payload['mensaje'], 
      uid: payload['de'],
      animationController: AnimationController(duration: const Duration(milliseconds: 200), vsync: this)
    );
 
    setState(() { 
      // agregar el mensaje al Array de texto de mensajes
      _messages.insert(0, newMessage);
    });
 
    // activamos el animation controller, lo dispara
    newMessage.animationController.forward();
  }


  
  @override
  Widget build(BuildContext context) {
    
    // accedemos al usuario que queremos enviarle el mensaje
    final usuarioPara = chatService.usuarioPara;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Column(
          children: [
            CircleAvatar(
              backgroundColor: Colors.blue[100],
              maxRadius: 14,
              child: Text(usuarioPara.nombre.substring(0, 2) , style: const TextStyle(fontSize: 12)),
            ),
            const SizedBox(height: 3),
            Text(usuarioPara.nombre, style: const TextStyle(color: Colors.black87, fontSize: 12))
          ],
        ),
        centerTitle: true,
        elevation: 1,
      ),
      body: Container(
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                itemCount: _messages.length,
                physics: const BouncingScrollPhysics(),
                itemBuilder: (_, index) => _messages[index], // esto es un Widget Function, parecido a un modelo
                reverse: true, // invierte el scroll para verlo tipo chat, coloca los de ultimo primeros
              ), 
            ),
            const Divider(height: 1),
            // caja de texto
            Container(
              color: Colors.white,
              // height: 100, // para pruebas
              child: _inputChat(),
            )
          ],
        ),
      ),
   );
  }

  Widget _inputChat() {
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: [
            Flexible(
              child: TextField(
                controller: _textController,
                onSubmitted: (texto) => 
                  texto.trim().isNotEmpty 
                    ?  _handleSubmit(texto)
                    : null,
                onChanged: (String texto){
                  // Cuando hay un valor para poder postear
                  setState(() {
                    if(texto.trim().isNotEmpty){
                      _estaEscribiendo = true;
                    }else{
                      _estaEscribiendo = false;
                    }
                  });
                },
                decoration: const InputDecoration.collapsed(
                  hintText: 'Enviar Mensaje' 
                ),
                focusNode: _focusNode,
              )
            ),

            // Boton de enviar del TextField
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: defaultTargetPlatform == TargetPlatform.iOS // tenia Platform.isIOS   ... defaultTargetPlatform me importo fundation
              ? CupertinoButton(
                onPressed: _estaEscribiendo
                  ? () => _handleSubmit(_textController.text.trim())
                  : null,
                child: const Text('Enviar')
              )
              : Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: IconTheme(
                  data: IconThemeData(color: Colors.blue[400]), // al asignar el color por aqui al desactivarlo se coloca en gris y activarlo se coloca en blue
                  child: IconButton(
                    highlightColor: Colors.transparent, // es el sombreado que aparece al hacer click en el boton
                    splashColor: Colors.transparent, // es el sombreado que aparece al hacer click en el boton
                    icon: const Icon(Icons.send),
                    onPressed: _estaEscribiendo
                      ? () => _handleSubmit(_textController.text.trim())
                      : null
                  ),
                ), 
              ),
            )

          ],
        ),
      )
    ); 
  } 

  _handleSubmit( String texto ) {
    // print('Soy el texto escrito: $texto');
    // limpia la caja de texto
    _textController.clear();

    // evita que se cierre el teclado al hacer enter
    _focusNode.requestFocus();

    // Aqui se carga el mensaje del mismo Enviador en su mismo telefono
    // Creamos la instancia del mensaje
    final newMessage =  ChatMessage(
      texto: texto, 
      uid: this.authService.usuarios.uid, // Quien envia el mensaje
      animationController: AnimationController(duration: const Duration(milliseconds: 200), vsync: this)
    );
    // agregar el mensaje al Array de texto de mensajes
    _messages.insert(0, newMessage); 
    // activamos el animation controller, lo dispara
    newMessage.animationController.forward();
    // al usar esta funcion ya termino de escribir por lo tanto seteo en false
    setState(() { _estaEscribiendo = false; });

    this.socketService.emit('mensaje-personal', {
      'de': this.authService.usuarios.uid, // este soy yooo OJO
      'para': this.chatService.usuarioPara.uid, // este es a quien le voy a mandar el mensaje
      'mensaje': texto
    });

  }

  @override
  void dispose() { 
    // Cancelar la escucha con el socket los mensajes personales
    // Eliminamos la escucha con el socket los mensajes personales al cerrar la pantalla
    // esto es para que no este escuchando si no tengo esta pantalla del chat abierta
    this.socketService.socket.off('mensaje-personal');
 
    // Limpiar instancia, al cerrar esta pantalla se va ejecutar lo que esta dentro del dispose
    for (ChatMessage message in _messages) {
      message.animationController.dispose();
    } 

    super.dispose();
  }
  
}