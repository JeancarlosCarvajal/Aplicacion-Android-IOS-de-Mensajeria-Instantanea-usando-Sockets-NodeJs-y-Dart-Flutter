import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:b_chat_flutter_app/services/auth_service.dart';
import 'package:b_chat_flutter_app/services/socket_service.dart';
import 'package:b_chat_flutter_app/services/chat_service.dart';
import 'package:b_chat_flutter_app/services/usuarios_service.dart';

import 'package:b_chat_flutter_app/models/usuario.dart';

class UsuariosPage extends StatefulWidget {
  const UsuariosPage({Key? key}) : super(key: key);

  @override
  State<UsuariosPage> createState() => _UsuariosPageState();
}

class _UsuariosPageState extends State<UsuariosPage> {

  // obtenemos la lista de usuarios. SIN USAR Provider
  final usuariosService = new UsuariosService();

  // documentacion del paquete de pull refresch https://pub.dev/packages/pull_to_refresh
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  // creamos la lista de usuarios
  late List<Usuario> usuarios = [];

  // Lista de usuarios para pruebas del diseno
  // final usuarios = [
  //   Usuario(online: true, email: 'test@gmail.com', nombre: 'Jean', uid: '1'),
  //   Usuario(online: false, email: 'test@gmail.com', nombre: 'Carlos', uid: '2'),
  //   Usuario(online: true, email: 'test@gmail.com', nombre: 'Lisa', uid: '3'),
  // ];

  // el init state se hace en los statefullwidgets para llamar funciones o metodos al inicial el Widget
  @override
  void initState() {
    // cargar los usuarios
    this._cargarUsuarios();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // acceder a provider
    final authService   = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);
    socketService.serverStatus;

    return Scaffold(
      appBar: AppBar( 
        title: Center(child: Text(authService.usuarios.nombre, style: const TextStyle(color: Colors.black87))),
        elevation: 1,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: (() {
            // Desconectamos el servidor
            socketService.disconnect();
            // Borramos el token de sesion JWT
            AuthService.deleToken();
            // Navegamos a la Pantalla de Login
            Navigator.pushReplacementNamed(context, 'login');
          }), 
          icon: const Icon(Icons.exit_to_app, color: Colors.black87)
        ),
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            child: socketService.serverStatus == ServerStatus.Online 
              ? const Icon(Icons.check_circle, color: Colors.blue)
              : const Icon(Icons.wifi_off_outlined, color: Colors.red),
          )
        ],
      ),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullDown: true,
        onRefresh: _cargarUsuarios,
        header: WaterDropHeader(
          complete: Icon(Icons.check, color: Colors.blue[400]),
          waterDropColor: Colors.blue,
        ),
        child: _listViewUsuarios(),
      ),
   );
  }

  ListView _listViewUsuarios() {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      itemBuilder: (_, index) => _usuarioListTile(usuarios[index]), 
      separatorBuilder: (context, index) => const Divider(), 
      itemCount: usuarios.length
    );
  }

  ListTile _usuarioListTile(Usuario usuario) {
    return ListTile(
      title: Text(usuario.nombre),
      subtitle: Text(usuario.email),
      leading: CircleAvatar(
        backgroundColor: Colors.blue[100],
        child: Text(usuario.nombre.substring(0,2)),
      ),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: usuario.online ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(100)
        ),
        // child: ,
      ),
      onTap: (){
        // print(usuario.nombre);
        // print(usuario.email);
        // accedemos al provider de chat Service
        final chatService = Provider.of<ChatService>(context, listen: false);
        // le damos el valor del usuario seleccionado al modelo del provider
        chatService.usuarioPara = usuario;
        // navegamos a la pantalla del chat con el usuario cargado en el modelo del provider
        Navigator.pushNamed(context, 'chat');
        
      },
    );
  }

  // esto permite cargar los elemtnos y que se pueda quitar el waterDrop y el circulod e espera
  _cargarUsuarios() async { 
    // print('cargando usuarios en flutter');
    
    // monitor network fetch
    // await Future.delayed(const Duration(milliseconds: 1000)); // para pruebas
    // if failed,use refreshFailed()
 
    // Retorna la lista de usuarios, OJO instancie esta clase arriba
    usuarios = await usuariosService.getUsuarios( inicio: '0' );
    setState(() { });
    // al llamar este metodo del paquete pull_to_refresh
    // me toma toda la lista de usuarios de la variable usuarios y reconstruye el Widget
    _refreshController.refreshCompleted();

  }


}
