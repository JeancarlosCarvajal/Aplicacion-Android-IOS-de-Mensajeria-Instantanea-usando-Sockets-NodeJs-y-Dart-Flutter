import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:b_chat_flutter_app/services/auth_service.dart';
import 'package:b_chat_flutter_app/services/socket_service.dart';

import 'package:b_chat_flutter_app/helpers/mostrar_alerta.dart';

import 'package:b_chat_flutter_app/widgets/custom_input.dart';
import 'package:b_chat_flutter_app/widgets/labels.dart';
import 'package:b_chat_flutter_app/widgets/logo.dart';
import 'package:b_chat_flutter_app/widgets/boton_azul.dart';



class RegisterPage extends StatelessWidget {

  const RegisterPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              const Logo( titulo: 'Registro' ),

              _Form(),

              const SizedBox(height: 30),
              
              const Labels( 
                ruta: 'login',
                titulo: '¿Ya tienes una cuenta?',
                subTitulo: 'Ingresa ahora!',
              ),

              const SizedBox(height: 10),


              const Text('Términos y condiciones de uso', style: TextStyle( fontWeight: FontWeight.w200 ),)

            ],
          ),
        ),
      )
   );
  }
}



class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {

  final nameCtrl  = TextEditingController();
  final emailCtrl = TextEditingController();
  final passCtrl  = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // llamemos el provider del servicio de conecion al backend
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context, listen: false);

    return Container(
      margin: const EdgeInsets.only(top: 40),
      padding: const EdgeInsets.symmetric( horizontal: 50 ),
       child: Column(
         children: <Widget>[
           
           CustomInput(
             icon: Icons.perm_identity,
             placeholder: 'Nombre',
             keyboardType: TextInputType.text, 
             textController: nameCtrl,
           ),

           CustomInput(
             icon: Icons.mail_outline,
             placeholder: 'Correo',
             keyboardType: TextInputType.emailAddress, 
             textController: emailCtrl,
           ),

           CustomInput(
             icon: Icons.lock_outline,
             placeholder: 'Contraseña',
             textController: passCtrl,
             isPassword: true,
           ),
           

           BotonAzul(
             text: 'Registrar',
             onPressed: authService.registrando
              ? null
              : () async {
              // print( emailCtrl.text );
              // print( passCtrl.text );

              // quita el teclado donde quiera que este
              FocusScope.of(context).unfocus();

              // llamamos la API para Logearnos
              final registerOk = await authService.register(nameCtrl.text.trim(), emailCtrl.text.trim(), passCtrl.text.trim());
              // this.usuario = registerOk

              // si el login esta bien navegar otra pantalla y me conecta a los Sockets
              if (registerOk == true) {
                // Conectamos al Socket
                socketService.connect();
                // Navega a la otra pantalla
                Navigator.pushReplacementNamed(context, 'usuarios');
              } else {
                // Mostar alerta
                mostrarAlerta(context, 'Registro incorrecto', '$registerOk');
              }
            },
           )

         ],
       ),
    );
  }
}
