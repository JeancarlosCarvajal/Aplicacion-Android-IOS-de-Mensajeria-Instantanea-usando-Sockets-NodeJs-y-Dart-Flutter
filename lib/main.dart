import 'package:flutter/material.dart';
import 'package:provider/provider.dart'; 

import 'package:b_chat_flutter_app/services/auth_service.dart';
import 'package:b_chat_flutter_app/services/socket_service.dart';
import 'package:b_chat_flutter_app/services/chat_service.dart';

import 'package:b_chat_flutter_app/routes/routes.dart';

void main() => runApp(const MyApp());
 
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService()), // crea instancia global  
        ChangeNotifierProvider(create: (_) => SocketService()), 
        ChangeNotifierProvider(create: (_) => ChatService()),  
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Chat App',
        initialRoute: 'loading',
        routes: appRoutes,
      ),
    );
  }
}


