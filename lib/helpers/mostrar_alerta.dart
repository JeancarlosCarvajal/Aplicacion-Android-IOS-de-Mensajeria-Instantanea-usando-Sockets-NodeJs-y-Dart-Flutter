// import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';



mostrarAlerta ( BuildContext context, String titulo, String subtitulo ) {

  if( defaultTargetPlatform == TargetPlatform.android ) { // si no es ... tenia !Platform.isIOS    ... defaultTargetPlatform me importo fundation
    return showDialog(
      context: context, 
      builder: (_) => AlertDialog(
          title: Text(titulo),
          content: Text(subtitulo),
          actions: [
            MaterialButton(
                onPressed: () => Navigator.pop(context), // cierra el dialogo
                textColor: Colors.blue,
                elevation: 5,
                child: const Text( 'Ok' ),
            )
          ],
      )
    );
  }
  
  // en caso que no sea Android sigue adelante entoences es IOS
  showCupertinoDialog(
    context: context, 
    builder: (_) => CupertinoAlertDialog(
      title: Text( titulo ),
      content: Text( subtitulo ),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text( 'Ok' ),
          onPressed: () => Navigator.pop(context), // cierra el dialogo
        )
      ],
    )
  );
   


}