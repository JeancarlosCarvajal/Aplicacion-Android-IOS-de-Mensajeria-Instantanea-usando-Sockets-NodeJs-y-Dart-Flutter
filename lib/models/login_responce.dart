// To parse this JSON data, do
//
//     final LoginResponce = LoginResponceFromMap(jsonString);

import 'dart:convert';

import 'package:b_chat_flutter_app/models/usuario.dart';

class LoginResponce {
  LoginResponce({
    required this.ok,
    required this.usuario,
    required this.token,
  });

  bool ok;
  Usuario usuario;
  String token;

  factory LoginResponce.fromJson(String str) =>
    LoginResponce.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LoginResponce.fromMap(Map<String, dynamic> json) => LoginResponce(
    ok: json["ok"],
    usuario: Usuario.fromMap(json["usuario"]),
    token: json["token"],
  );

  Map<String, dynamic> toMap() => {
    "ok": ok,
    "usuario": usuario.toMap(),
    "token": token,
  };
}
 